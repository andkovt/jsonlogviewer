﻿using System.Threading.Tasks;

namespace AKovtuns.JsonLogViewer.FileReaders
{
    /// <summary>
    ///     Responsible for reading log lines from a file
    /// </summary>
    public interface IFileReader
    {
        /// <summary>
        ///     Checks if it's an end of the file
        /// </summary>
        bool EndOfFile { get; }

        /// <summary>
        ///     Reads the next line in file
        /// </summary>
        /// <returns>Log line</returns>
        Task<string> ReadlineAsync();
    }
}