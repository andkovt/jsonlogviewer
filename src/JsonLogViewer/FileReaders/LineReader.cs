﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AKovtuns.JsonLogViewer.FileReaders
{
    public class LineReader
    {
        private string unfinishedLine = string.Empty;
        private byte[] currentBytes;
        private byte[] currentNextBytes;

        public IList<string> Read(byte[] bytes, byte[] nextBytes)
        {
            currentBytes = bytes;
            currentNextBytes = nextBytes;
            
            var contents = Encoding.Default.GetString(bytes);
            Debugger.Write($"Reading '{contents}'");
            
            var lines = Split(contents);
            if (lines.Count == 0)
            {
                throw new Exception("Failed to read lines, no lines found");
            }
            
            var outputLines = new List<string>();
            int count = 1;
            foreach (var line in lines)
            {
                Debugger.Write($"Processing line '{line}'");
                ProcessLine(line, outputLines, (count == lines.Count));
                count++;
            }
            
            return outputLines;
        }

        private void ProcessLine(string line, IList<string> outputLines, bool lastLine = false)
        {
            // Check if it's a start of a log message
            if (line.StartsWith("{"))
            {
                unfinishedLine = string.Empty; // Clear any previous unfinished lines, it's sad if end was not found
                
                // Check that we actually have a whole message
                if (line.EndsWith("}"))
                {
                    // Check if ending is cut right at then } character
                    if (lastLine && !AssertEndingNotCut())
                    {
                        Debugger.Write("Line ends with a } character, but no new line. Marking as an unfinished line");
                        unfinishedLine = line;
                        return;
                    }
                    
                    Debugger.Write("Processed as a whole line");
                    outputLines.Add(line);
                    return;
                }
                
                // It's not a whole message
                unfinishedLine += line;
                return;
            }
            
            // Check if have any unfinished log messages, append contents of this if we have
            if (!string.IsNullOrEmpty(unfinishedLine))
            {
                unfinishedLine += line;
                Debugger.Write("Line combined with a previous unfinished line");

                // Check if we found the end of the unfinished line
                if (unfinishedLine.EndsWith("}"))
                {
                    // Check if ending is cut right at then } character
                    if (lastLine && !AssertEndingNotCut())
                    {
                        Debugger.Write(
                            "Line ends with a } character, but no new line. Marking as a unfinished line. Combined with a previous unfinished line"
                        );
                        return;
                    }

                    Debugger.Write("Finished unfinished line");
                    outputLines.Add(unfinishedLine);
                    unfinishedLine = string.Empty;
                    return;
                }
            }
            
            // Line has no start and we don't have any unfinished lines, discard this line
            Debugger.Write("Line discarded");
        }

        private bool AssertEndingNotCut()
        {
            if (currentNextBytes.Length == 0)
            {
                return true;
            }
            
            // Check if current byte buffer ends with a LF or CR
            if (currentBytes.Last() == '\n' || currentBytes.Last() == '\r')
            {
                return true;
            }

            // Peek at the next bytes
            if (currentNextBytes.First() == '\n' || currentNextBytes.First() == '\r')
            {
                return true;
            }

            return false; // No new line in sight
        }
        
        private IList<string> Split(string contents)
        {
            var lines = new List<string>();
            string? line = null;

            using var stringReader = new StringReader(contents);
            while ((line = stringReader.ReadLine()) != null)
            {
                lines.Add(line);
            }

            return lines;
        }
    }
}