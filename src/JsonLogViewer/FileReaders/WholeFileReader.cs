﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace AKovtuns.JsonLogViewer.FileReaders
{
    public class WholeFileReader : IFileReader, IDisposable
    {
        private readonly string filePath;
        private readonly FileStream fileStream;
        private readonly StreamReader streamReader;

        public WholeFileReader(string filePath)
        {
            Debugger.Write("Initializing WholeFileReader");

            this.filePath = filePath;
            fileStream = Utils.AssertAndOpenFile(filePath);
            streamReader = new StreamReader(fileStream);
        }

        public void Dispose()
        {
            fileStream?.Dispose();
            streamReader?.Dispose();
        }

        public bool EndOfFile => streamReader.EndOfStream;

        public async Task<string> ReadlineAsync()
        {
            Debugger.Write("Attempting to read a next line from a file");

            if (EndOfFile) throw new Exception("Tried to read log file when it's the end of the file");
            var line = await streamReader.ReadLineAsync();
            Debugger.Write($"Read from file {line}");
            return line;
        }
    }
}