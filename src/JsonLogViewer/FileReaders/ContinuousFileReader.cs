﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AKovtuns.JsonLogViewer.FileReaders
{
    public class ContinuousFileReader : IFileReader
    {
        private const int ReadingBuffer = 1024;
        private const int LengthRetrievingMaxAttempts = 10;
        private const int WriteWaitTimeMs = 100;

        private readonly FileStream fileStream;
        private readonly Queue<string> lineQueue = new Queue<string>();
        private readonly LineReader lineReader;
        private long lastLength;
        private long lastPosition;
        private bool readingBegan;

        public ContinuousFileReader(string filePath)
        {
            Debugger.Write("Initializing ContinuousFileReader");
            fileStream = Utils.AssertAndOpenFile(filePath);
            lineReader = new LineReader();
        }

        public bool EndOfFile => false;

        public async Task<string> ReadlineAsync()
        {
            Debugger.Write("Attempting to read a next line from a file");

            if (!readingBegan) StartReading(); // Prepare for reading
            if (lineQueue.Count > 0) return lineQueue.Dequeue(); // We already have lines in queue, return those

            IList<string> readLines = new List<string>();

            while (readLines.Count == 0)
            {
                WaitForUpdate();
                var bufferSize = GetNextBufferSize();

                fileStream.Seek(lastPosition, SeekOrigin.Begin);
                var buffer = new byte[bufferSize];
                fileStream.Read(buffer, 0, bufferSize);
                lastPosition = lastPosition + bufferSize;
                
                readLines = lineReader.Read(buffer, PeekNext(2));
            }

            foreach (var readLine in readLines)
            {
                Debugger.Write($"Read Line: {readLine}");
                lineQueue.Enqueue(readLine);
            }

            return lineQueue.Dequeue();
        }

        /// <summary>
        ///     Initializes reading procedure
        /// </summary>
        private void StartReading()
        {
            readingBegan = true;

            long endOffset = ReadingBuffer;
            var length = GetStreamLength();
            if (length < ReadingBuffer) endOffset = length;

            lastPosition = length - endOffset;
        }

        /// <summary>
        ///     Calculates the size of the next read buffer
        /// </summary>
        /// <returns></returns>
        private int GetNextBufferSize()
        {
            var bufferSize = ReadingBuffer;
            var fileStreamLength = GetStreamLength();

            if (bufferSize > fileStreamLength - lastPosition)
                bufferSize = (int) (fileStreamLength - lastPosition);

            return bufferSize;
        }

        /// <summary>
        ///     Checks if we reached EOF, waits for file to be updated
        /// </summary>
        private void WaitForUpdate()
        {
            while (GetStreamLength() <= lastPosition) Thread.Sleep(WriteWaitTimeMs);
        }

        /// <summary>
        ///     Returns length of the opened file stream
        ///     Sometimes when file is written fileStream.length can return 0, this method tried to not fall in that trap
        /// </summary>
        /// <returns></returns>
        private long GetStreamLength()
        {
            var length = fileStream.Length;

            // All good
            if (length > 0)
            {
                lastLength = length;
                return length;
            }

            // Last file length also was 0, probably empty file
            if (lastLength == 0) return length;

            // Attempt to get correct length x times
            for (var i = 0; i < LengthRetrievingMaxAttempts; i++)
            {
                length = fileStream.Length;
                if (length > 0) break;

                Thread.Sleep(50);
            }

            // What we got is probably correct
            lastLength = length;
            return length;
        }

        private byte[] PeekNext(int bytes)
        {
            // EOF nothing to peek
            if (lastPosition + bytes >= GetStreamLength())
            {
                return new byte[0];
            }

            var peekBuffer = new byte[bytes];
            fileStream.Read(peekBuffer, 0, bytes);
            fileStream.Seek(-bytes, SeekOrigin.Current);

            return peekBuffer;
        }
    }
}