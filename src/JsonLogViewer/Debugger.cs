﻿using System;
using CliFx;

namespace AKovtuns.JsonLogViewer
{
    public static class Debugger
    {
        /// <summary>
        /// Output console
        /// </summary>
        public static IConsole Console { get; set; }
        
        /// <summary>
        /// Toggles debug output
        /// </summary>
        public static bool Enabled { get; set; }
        
        /// <summary>
        /// Writes debug message to console
        /// </summary>
        /// <param name="message">Debug message</param>
        public static void Write(string message)
        {
            if (!Enabled) return;
            Console.WithForegroundColor(ConsoleColor.DarkGray, () => Console.Output.WriteLine($"[Verbose] {message}"));
        }
    }
}