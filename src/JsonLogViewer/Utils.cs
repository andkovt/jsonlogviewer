﻿using System.IO;

namespace AKovtuns.JsonLogViewer
{
    public static class Utils
    {
        /// <summary>
        /// Asserts that file exists and opens it for reading
        /// </summary>
        /// <param name="filePath">File to open</param>
        /// <returns>File stream</returns>
        /// <exception cref="FileNotFoundException"></exception>
        public static FileStream AssertAndOpenFile(string filePath)
        {
            Debugger.Write($"Trying to open file {filePath}");

            if (!File.Exists(filePath)) throw new FileNotFoundException("Could not open log file", filePath);
            return File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        }
    }
}