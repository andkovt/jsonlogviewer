﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AKovtuns.JsonLogViewer.FileReaders;
using AKovtuns.JsonLogViewer.Output;
using AKovtuns.JsonLogViewer.Parsing;
using AKovtuns.JsonLogViewer.Parsing.Lookup;
using CliFx;
using CliFx.Attributes;

namespace AKovtuns.JsonLogViewer
{
    [Command]
    public class Command : ICommand
    {
        private IFileReader fileReader;
        private ILineParser lineParser;
        private ILogOutput logOutput;

        [CommandParameter(0, Description = "Path to the log file", Name = "Log file")]
        public string File { get; set; }

        [CommandOption("watch-file", 'w', Description = "Watch file for changes")]
        public bool WatchFile { get; set; }

        [CommandOption("verbose", 'v', Description = "Enable verbose output")]
        public bool Verbose { get; set; }

        [CommandOption("additional-field", 'a', Description = "Additional fields to retrieve from the log")]
        public List<string> AdditionalFields { get; set; } = new List<string>();

        public async ValueTask ExecuteAsync(IConsole console)
        {
            if (Verbose)
            {
                Debugger.Console = console;
                Debugger.Enabled = true;

                Debugger.Write("Enabled verbose output");
            }

            fileReader = WatchFile ? (IFileReader) new ContinuousFileReader(File) : new WholeFileReader(File);
            lineParser = new LineParser(AssembleParsingConfig());
            logOutput = new ConsoleLogOutput(console);

            while (!fileReader.EndOfFile)
            {
                var line = await fileReader.ReadlineAsync();
                var parsedLine = lineParser.Parse(line);
                logOutput.Write(parsedLine);
            }
        }

        private ParsingConfig AssembleParsingConfig()
        {
            // Default config
            var defaultConfig = new ParsingConfig
            {
                Time = AssembleTimeLookup(),
                Message = AssembleMessageLookup()
            };

            foreach (var additionalField in AdditionalFields)
                defaultConfig.AddField(additionalField, new PathLookup(additionalField));

            return defaultConfig;
        }

        private IFieldLookup AssembleTimeLookup()
        {
            return new MultiPathLookup(new List<IFieldLookup>
            {
                new PathLookup("datetime"),
                new PathLookup("timestamp"),
                new PathLookup("time"),
                new PathLookup("date")
            });
        }

        private IFieldLookup AssembleMessageLookup()
        {
            return new MultiPathLookup(new List<IFieldLookup>
            {
                new PathLookup("message"),
                new PathLookup("msg"),
                new PathLookup("data"),
            });
        }
    }
}