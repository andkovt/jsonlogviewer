﻿using System.Collections.Generic;

namespace AKovtuns.JsonLogViewer.Parsing
{
    public class ParsedLine
    {
        public string Time { get; set; }
        public string Message { get; set; }
        public IDictionary<string, string> AdditionalFields { get; } = new Dictionary<string, string>();
    }
}