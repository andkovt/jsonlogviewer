﻿using Newtonsoft.Json.Linq;

namespace AKovtuns.JsonLogViewer.Parsing.Lookup
{
    public class PathLookup : IFieldLookup
    {
        private readonly string jsonField;

        public PathLookup(string jsonField)
        {
            this.jsonField = jsonField;
        }

        public string Lookup(JObject json)
        {
            return (string) json.SelectToken(jsonField);
        }
    }
}