﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace AKovtuns.JsonLogViewer.Parsing.Lookup
{
    public class MultiPathLookup : IFieldLookup
    {
        private readonly IList<IFieldLookup> fieldLookups;

        public MultiPathLookup(IList<IFieldLookup> fieldLookups)
        {
            this.fieldLookups = fieldLookups;
        }

        public string Lookup(JObject json)
        {
            foreach (var fieldLookup in fieldLookups)
            {
                var result = fieldLookup.Lookup(json);
                if (!string.IsNullOrEmpty(result)) return result;
            }

            return string.Empty;
        }
    }
}