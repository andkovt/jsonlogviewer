﻿using Newtonsoft.Json.Linq;

namespace AKovtuns.JsonLogViewer.Parsing.Lookup
{
    public interface IFieldLookup
    {
        string Lookup(JObject json);
    }
}