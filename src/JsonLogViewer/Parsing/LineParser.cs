﻿using System;
using Newtonsoft.Json.Linq;

namespace AKovtuns.JsonLogViewer.Parsing
{
    public class LineParser : ILineParser
    {
        private readonly ParsingConfig config;

        public LineParser(ParsingConfig config)
        {
            this.config = config;
        }

        public ParsedLine Parse(string line)
        {
            try
            {
                var parsedObject = JObject.Parse(line);
                var parsedLine = new ParsedLine();

                parsedLine.Time = config.Time.Lookup(parsedObject);
                parsedLine.Message = config.Message.Lookup(parsedObject);

                foreach (var additionalField in config.AdditionalFields)
                    parsedLine.AdditionalFields.Add(additionalField.Key, additionalField.Value.Lookup(parsedObject));

                return parsedLine;
            }
            catch (Exception e)
            {
                throw e;
            }

            
        }
    }
}