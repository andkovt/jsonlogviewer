﻿using System.Collections.Generic;
using AKovtuns.JsonLogViewer.Parsing.Lookup;

namespace AKovtuns.JsonLogViewer.Parsing
{
    public class ParsingConfig
    {
        public IFieldLookup Time { get; set; }
        public IFieldLookup Message { get; set; }
        public Dictionary<string, IFieldLookup> AdditionalFields { get; } = new Dictionary<string, IFieldLookup>();

        public void AddField(string fieldName, IFieldLookup lookup)
        {
            AdditionalFields.Add(fieldName, lookup);
        }
    }
}