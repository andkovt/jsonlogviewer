﻿namespace AKovtuns.JsonLogViewer.Parsing
{
    public interface ILineParser
    {
        ParsedLine Parse(string line);
    }
}