﻿using AKovtuns.JsonLogViewer.Parsing;

namespace AKovtuns.JsonLogViewer.Output
{
    public interface ILogOutput
    {
        void Write(ParsedLine line);
    }
}