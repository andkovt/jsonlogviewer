﻿using System;
using AKovtuns.JsonLogViewer.Parsing;
using CliFx;

namespace AKovtuns.JsonLogViewer.Output
{
    public class ConsoleLogOutput : ILogOutput
    {
        private const int Col1Size = 30;
        private const int ColumnSpace = 1;

        private readonly IConsole console;

        private ConsoleColor col1Color = ConsoleColor.Gray;
        private ConsoleColor col2Color = ConsoleColor.Gray;

        public ConsoleLogOutput(IConsole console)
        {
            this.console = console;
        }

        public void Write(ParsedLine line)
        {
            // Write time & message
            SetRowColors(ColorSettings.TimeColor, ColorSettings.Message);
            WriteRow(line.Time, line.Message);

            // Write additional fields
            foreach (var additionalField in line.AdditionalFields)
            {
                SetRowColors(ColorSettings.AdditionalFieldName, ColorSettings.AdditionalFieldValue);
                WriteRow(additionalField.Key, additionalField.Value);
            }
        }

        private void SetRowColors(ConsoleColor col1Color, ConsoleColor col2Color)
        {
            this.col1Color = col1Color;
            this.col2Color = col2Color;
        }

        private void WriteRow(string col1Value, string col2Value)
        {
            var columnSpacing = "".PadLeft(ColumnSpace);
            col1Value = PadLeft(col1Value, Col1Size);

            console.WithForegroundColor(col1Color, () => console.Output.Write(col1Value));
            console.Output.Write(columnSpacing);
            console.WithForegroundColor(col2Color, () => console.Output.Write(col2Value));

            console.Output.WriteLine();
        }
        
        private string PadLeft(string value, int length)
        {
            var padAmount = length - value.Length;
            if (padAmount <= 0) return value;

            return value.PadLeft(length);
        }
    }
}