﻿using System;

namespace AKovtuns.JsonLogViewer.Output
{
    public static class ColorSettings
    {
        public static ConsoleColor TimeColor { get; set; } = ConsoleColor.Gray;
        public static ConsoleColor Message { get; set; } = ConsoleColor.Gray;
        public static ConsoleColor AdditionalFieldName { get; set; } = ConsoleColor.Yellow;
        public static ConsoleColor AdditionalFieldValue { get; set; } = ConsoleColor.Gray;
    }
}